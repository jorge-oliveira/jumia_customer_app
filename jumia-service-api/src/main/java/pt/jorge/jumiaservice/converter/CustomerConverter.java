package pt.jorge.jumiaservice.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;
import pt.jorge.jumiaservice.dto.CustomerDTO;
import pt.jorge.jumiaservice.dto.PageDTO;
import pt.jorge.jumiaservice.model.Customer;
import pt.jorge.jumiaservice.model.SearchFilter;
import pt.jorge.jumiaservice.service.CountryService;
import pt.jorge.jumiaservice.service.PhoneService;
import pt.jorge.jumiaservice.service.StateService;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class CustomerConverter {

    @Autowired
    private PhoneService phoneService;

    @Autowired
    private CountryService countryService;

    @Autowired
    private StateService stateService;

    public PageDTO<CustomerDTO> convertPageCustomersEntityToDto(SearchFilter phone, Page<Customer> customers) {
        List<CustomerDTO> customersList = customers.stream()
                .map(this::convertCustomerEntityToDto)
                .collect(Collectors.toList());

        removeElementByState(phone, customersList);

        return new PageDTO<>(customersList, customers.getTotalElements(), customers.getTotalPages(), customers.getNumber(), customers.getSize());
    }

    public CustomerDTO convertCustomerEntityToDto(Customer customer) {

        CustomerDTO customerDto = new CustomerDTO();
        customerDto.setId(customer.getId());
        customerDto.setName(customer.getName().trim().toUpperCase());
        customerDto.setPhoneNumber(customer.getPhone());
        customerDto.setCountryCode(Integer.parseInt(phoneService.extractPhoneCode(customer.getPhone())));
        customerDto.setCountryName(countryService.findCountry(customerDto.getCountryCode()));
        customerDto.setStateValid(stateService.validateState(customerDto.getCountryName(), customerDto.getPhoneNumber()));

        return customerDto;
    }

    private void removeElementByState(SearchFilter phone, List<CustomerDTO> customersList) {
        if (phone.isValid() != null) {
            customersList.removeIf(state -> state.isStateValid() != phone.isValid());
        }
    }
}
