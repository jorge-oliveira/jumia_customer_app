package pt.jorge.jumiaservice.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import pt.jorge.jumiaservice.dto.CountryDTO;
import pt.jorge.jumiaservice.enums.CountryEnum;
import pt.jorge.jumiaservice.service.CountryService;

import java.util.EnumMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class CountryServiceImpl implements CountryService {

    final static Logger logger = LoggerFactory.getLogger(CountryServiceImpl.class);

    @Override
    public String findCountry(Integer countryCode) {

        logger.info("Finding country for phone number: {}", countryCode);
        String country = null;
        switch (countryCode) {
            case 237:
                country = CountryEnum.CAMEROON.getName();
                break;
            case 251:
                country = CountryEnum.ETHIOPIA.getName();
                break;
            case 212:
                country = CountryEnum.MAROCCO.getName();
                break;
            case 258:
                country = CountryEnum.MOZAMBIQUE.getName();
                break;
            case 256:
                country = CountryEnum.UGANDA.getName();
                break;
            default:
                break;
        }

        return country;
    }

    @Override
    public List<CountryDTO> getAllCountries() {

        EnumMap<CountryEnum, Integer> enumMap = new EnumMap<>(CountryEnum.class);
        enumMap.put(CountryEnum.CAMEROON, 237);
        enumMap.put(CountryEnum.ETHIOPIA, 251);
        enumMap.put(CountryEnum.MAROCCO, 212);
        enumMap.put(CountryEnum.MOZAMBIQUE, 258);
        enumMap.put(CountryEnum.UGANDA, 256);

        Stream<CountryDTO> countryStream = enumMap.entrySet().stream().map(countryEnumIntegerEntry -> {

            CountryDTO country = new CountryDTO();

            country.setCode(countryEnumIntegerEntry.getValue());
            country.setName(countryEnumIntegerEntry.getKey().getName());
            return country;
        });

        return countryStream.collect(Collectors.toList());
    }
}
