package pt.jorge.jumiaservice.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import pt.jorge.jumiaservice.service.PhoneService;

@Service
public class PhoneServiceImpl implements PhoneService {

    final static Logger logger = LoggerFactory.getLogger(PhoneServiceImpl.class);

    @Override
    public String extractPhoneCode(String phoneNumber) {

        logger.info("Extracting indicative of phoneNumber {}", phoneNumber);

        String code = null;
        try {

            int startPos = phoneNumber.indexOf("(");
            int endPos = phoneNumber.indexOf(")");

            code = phoneNumber.substring(startPos + 1, endPos).trim();
            logger.debug("code: {}", code);

        } catch (StringIndexOutOfBoundsException str) {
            logger.info("Error: invalid phone code {} structure", phoneNumber);
        }

        return code;
    }
}
