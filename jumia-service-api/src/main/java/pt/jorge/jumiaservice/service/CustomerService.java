package pt.jorge.jumiaservice.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import pt.jorge.jumiaservice.exception.CustomerNotFoundException;
import pt.jorge.jumiaservice.model.Customer;
import pt.jorge.jumiaservice.model.SearchFilter;

import java.util.List;

public interface CustomerService {

    List<Customer> fetchAllCustomer();

    Page<Customer> fetchCustomerByFilter(SearchFilter filter, Pageable pageable) throws CustomerNotFoundException;
}
