package pt.jorge.jumiaservice.service;

public interface PhoneService {

    String extractPhoneCode(String phoneNumber);
}
