package pt.jorge.jumiaservice.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import pt.jorge.jumiaservice.service.StateService;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class StateServiceImpl implements StateService {

    final static Logger logger = LoggerFactory.getLogger(StateServiceImpl.class);

    @Override
    public boolean validateState(String country, String phoneNumber) {
        logger.info("Checking if the phone number {} is valid.", phoneNumber);
        return validatePhoneNumber(country, phoneNumber);
    }

    private boolean validatePhoneNumber(String country, String phoneNumber) {

        logger.debug("validating the phoneNumber: {}", phoneNumber);
        // find the last space index
        String regex = null;

        switch (country) {
            case "Cameroon":
                regex = "\\(237\\)\\ ?[2368]\\d{7,8}$";
                break;
            case "Ethiopia":
                regex = "\\(251\\)\\ ?[1-59]\\d{8}$";
                break;
            case "Morocco":
                regex = "\\(212\\)\\ ?[5-9]\\d{8}$";
                break;
            case "Mozambique":
                regex = "\\(258\\)\\ ?[28]\\d{7,8}$";
                break;
            case "Uganda":
                regex = "\\(256\\)\\ ?\\d{9}$";
                break;

            default:
                regex = "-";
                logger.error("Cannot find country code for {} phone number.", phoneNumber);
                break;
        }

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(phoneNumber);

        if (matcher.find()) {
            return true;
        } else {
            return false;
        }
    }
}
