package pt.jorge.jumiaservice.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import pt.jorge.jumiaservice.exception.CustomerNotFoundException;
import pt.jorge.jumiaservice.model.Customer;
import pt.jorge.jumiaservice.model.SearchFilter;
import pt.jorge.jumiaservice.repository.CustomerRepository;
import pt.jorge.jumiaservice.service.CountryService;
import pt.jorge.jumiaservice.service.CustomerService;
import pt.jorge.jumiaservice.service.PhoneService;
import pt.jorge.jumiaservice.service.StateService;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class CustomerServiceImpl implements CustomerService {

    final static Logger logger = LoggerFactory.getLogger(CustomerServiceImpl.class);

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    CountryService countryService;

    @Autowired
    PhoneService phoneService;

    @Autowired
    StateService stateService;

    @Override
    public List<Customer> fetchAllCustomer() {
        logger.info("Getting Customer list.");

        Iterable<Customer> customers = customerRepository.findAll();
        List<Customer> lstCustomers = StreamSupport.stream(customers.spliterator(), false)
                .collect(Collectors.toList());

        return lstCustomers;
    }

    @Override
    public Page<Customer> fetchCustomerByFilter(SearchFilter filter, Pageable pageable) throws CustomerNotFoundException {
        logger.info("Start listing the customers with {} filter.", filter);

        PageRequest pageRequest = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Sort.by("id"));
        if (StringUtils.isEmpty(filter.getCode())) {
            filter.setCode("%");
        }
        Page<Customer> customers = customerRepository.findAllCustomersByPhoneNumberCode(filter.getCode(), pageRequest);
        if (customers == null || customers.getTotalElements() == 0){
            logger.warn("No customer found with the filter {}", filter);
            throw new CustomerNotFoundException("No customer found.");
        }

        return customers;
    }

}
