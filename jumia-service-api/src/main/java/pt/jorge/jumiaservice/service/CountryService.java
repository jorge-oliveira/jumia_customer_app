package pt.jorge.jumiaservice.service;

import pt.jorge.jumiaservice.dto.CountryDTO;

import java.util.List;

public interface CountryService {

    String findCountry(Integer countryCode);

    List<CountryDTO> getAllCountries();
}
