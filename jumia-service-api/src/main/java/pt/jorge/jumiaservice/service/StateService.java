package pt.jorge.jumiaservice.service;

public interface StateService {

    boolean validateState(String countryName, String phoneNumber);
}
