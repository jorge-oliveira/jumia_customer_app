package pt.jorge.jumiaservice.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import pt.jorge.jumiaservice.dto.CountryDTO;
import pt.jorge.jumiaservice.service.CountryService;

import java.util.List;

@Tag(name = "Country", description = "List all Countries")
@RestController
@RequestMapping(value = "/api/v1/country")
@CrossOrigin("*")
public class CountryController {

    @Autowired
    CountryService countryService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<List<CountryDTO>> listAllCountries(){

        List<CountryDTO> lstCountries = countryService.getAllCountries();

        return ResponseEntity.ok(lstCountries);
    }
}
