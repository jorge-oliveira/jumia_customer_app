package pt.jorge.jumiaservice.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pt.jorge.jumiaservice.converter.CustomerConverter;
import pt.jorge.jumiaservice.dto.CustomerDTO;
import pt.jorge.jumiaservice.dto.PageDTO;
import pt.jorge.jumiaservice.model.Customer;
import pt.jorge.jumiaservice.model.SearchFilter;
import pt.jorge.jumiaservice.service.CustomerService;

import java.util.List;
import java.util.stream.Collectors;

@Tag(name = "Customers", description = "List all customers")
@RestController
@RequestMapping(value = "/api/v1/customer")
//@CrossOrigin("http://localhost:4200/")
@CrossOrigin("*")
public class CustomerController {

    final static Logger logger = LoggerFactory.getLogger(CustomerController.class);

    @Autowired
    CustomerService customerService;

    @Autowired
    CustomerConverter customerConverter;

    @Operation(summary = "Fetch Costumer phones", description = "Get a list of all the costumers.")
    @RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<CustomerDTO>> fetchAllCustomer() {
        logger.info("Getting the all customers.");

        List<CustomerDTO> collect = customerService.fetchAllCustomer().stream()
                .map(customerConverter::convertCustomerEntityToDto)
                .collect(Collectors.toList());

        return new ResponseEntity<>(collect, HttpStatus.OK);
    }

    @Operation(summary = "Fetch All Costumer by filter", description = "Listing countries phone numbers with pagination.")
    @RequestMapping(value = "", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PageDTO<CustomerDTO>> fetchCustomerByFilter(@RequestBody(required = false) SearchFilter filter, Pageable pageable) {
        logger.info("Listing customers phone numbers by {} country phone numbers.", filter);

        try {

            Page<Customer> customers = customerService.fetchCustomerByFilter(filter, pageable);
            PageDTO<CustomerDTO> customerDTOPageDTO = customerConverter.convertPageCustomersEntityToDto(filter, customers);

            return new ResponseEntity<>(customerDTOPageDTO, HttpStatus.OK);

        }catch (Exception e){
            logger.debug("Exception: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

}
