package pt.jorge.jumiaservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JumiaServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(JumiaServiceApplication.class, args);
	}

}
