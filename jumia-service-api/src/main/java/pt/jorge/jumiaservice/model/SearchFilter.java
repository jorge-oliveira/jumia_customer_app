package pt.jorge.jumiaservice.model;

import lombok.Data;

@Data
public class SearchFilter {
    private String code;
    private Boolean valid;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Boolean isValid() {
        return valid;
    }

    public void setValid(Boolean valid) {
        this.valid = valid;
    }
}
