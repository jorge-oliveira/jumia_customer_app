package pt.jorge.jumiaservice.dto;

import lombok.Data;

@Data
public class CodeDTO {
    private int code;
}
