package pt.jorge.jumiaservice.dto;

import lombok.Data;

@Data
public class CountryDTO extends CodeDTO {
    private String name;
}
