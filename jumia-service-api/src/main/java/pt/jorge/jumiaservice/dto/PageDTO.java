package pt.jorge.jumiaservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class PageDTO<T> {

    List<T> content;

    CustomPageDTO customPage;

    public PageDTO(List<T> content, Long totalElements, int totalPages, int number, int size ) {
        this.content = content;
        this.customPage = new CustomPageDTO(totalElements,
                totalPages, number, size);
    }

}
