package pt.jorge.jumiaservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CustomPageDTO {
    Long totalElements;

    int totalPages;

    int number;

    int size;
}
