package pt.jorge.jumiaservice.dto;

import lombok.Data;

@Data
public class CustomerDTO{

    private Integer id;
    private String name;
    private String phoneNumber;
    private int countryCode;
    private String countryName;
    private boolean stateValid;
}
