package pt.jorge.jumiaservice.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import pt.jorge.jumiaservice.model.Customer;

@Repository
public interface CustomerRepository extends PagingAndSortingRepository<Customer, Integer> , JpaSpecificationExecutor<Customer> {

    @Query("select c from Customer c where c.phone LIKE CONCAT('(',:code ,')%')")
    Page<Customer> findAllCustomersByPhoneNumberCode(String code, Pageable pageRequest);
}
