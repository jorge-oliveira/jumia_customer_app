package pt.jorge.jumiaservice.enums;

public enum CountryEnum {

    CAMEROON("Cameroon"),
    ETHIOPIA("Ethiopia"),
    MAROCCO("Morocco"),
    MOZAMBIQUE("Mozambique"),
    UGANDA("Uganda");

    private String name;

    CountryEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
