package pt.jorge.jumiaservice.repository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import pt.jorge.jumiaservice.model.Customer;
import pt.jorge.jumiaservice.model.SearchFilter;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@AutoConfigureMockMvc
class CustomerRepositoryTest {


    @Autowired
    CustomerRepository customerRepository;

    private SearchFilter filter;
    private Pageable pageRequest;

    @BeforeEach
    void setUp() {
        pageRequest = PageRequest.of(0, 50, Sort.by("id"));
        filter = new SearchFilter();
    }

    @Test
    void findAllCustomersByPhoneNumberCodeValidTest() {
        filter.setCode("212");

        Page<Customer> allByPhoneNumber = customerRepository.findAllCustomersByPhoneNumberCode(filter.getCode(), pageRequest);
        assertTrue(allByPhoneNumber.getContent().size() > 0);
    }

    @Test
    void findAllCustomersByPhoneNumberCodeInvalidTest() {
        filter.setCode("BAD");

        Page<Customer> allByPhoneNumber = customerRepository.findAllCustomersByPhoneNumberCode(filter.getCode(), pageRequest);
        assertFalse(allByPhoneNumber.getContent().size() > 0);
    }

    @Test
    void findAllCustomersByPhoneNumberCodePaginationTest() {
        filter.setCode("212");
        pageRequest = PageRequest.of(0, 3, Sort.by("id"));

        Page<Customer> allByPhoneNumber = customerRepository.findAllCustomersByPhoneNumberCode(filter.getCode(), pageRequest);
        assertEquals(3, allByPhoneNumber.getContent().size());
    }

    @Test
    void findAllCustomersByPhoneNumberCodePaginationAllResultsTest() {
        filter.setCode("%");

        Page<Customer> allByPhoneNumber = customerRepository.findAllCustomersByPhoneNumberCode(filter.getCode(), pageRequest);
        assertEquals(41, allByPhoneNumber.getContent().size());
    }
}