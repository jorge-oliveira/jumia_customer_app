package pt.jorge.jumiaservice.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import pt.jorge.jumiaservice.converter.CustomerConverter;
import pt.jorge.jumiaservice.dto.CustomerDTO;
import pt.jorge.jumiaservice.dto.PageDTO;
import pt.jorge.jumiaservice.exception.CustomerNotFoundException;
import pt.jorge.jumiaservice.model.Customer;
import pt.jorge.jumiaservice.model.SearchFilter;
import pt.jorge.jumiaservice.service.CustomerService;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class CustomerControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Mock
    CustomerService customerService;

    @Mock
    CustomerConverter customerConverter;

    @InjectMocks
    private CustomerController customerController;

    private PageRequest pageRequest;
    private SearchFilter filter;
    private final List<CustomerDTO> listCustomersDTO = new ArrayList<>();
    private final List<Customer> lstCustomer = new ArrayList<>();

    @BeforeEach
    void setUp() {
        pageRequest = PageRequest.of(0, 1, Sort.by("id"));
        filter = new SearchFilter();

    }

    @Test
    void customerUrlTest() throws Exception {
        this.mockMvc.perform(get("/api/v1/customer"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void fetchAllCustomerTest() {

        // create a list of customers
        Customer customer1 = new Customer();
        customer1.setId(1);
        customer1.setName("Ze Pincel");
        customer1.setPhone("(351) 2254565");

        Customer customer2 = new Customer();
        customer2.setId(2);
        customer2.setName("Ze Maria");
        customer2.setPhone("(351) 2254565");

        lstCustomer.add(customer1);
        lstCustomer.add(customer2);

        when(this.customerService.fetchAllCustomer())
                .thenReturn(lstCustomer);

        // create a list of customersDto
        CustomerDTO customerDTO1 = new CustomerDTO();
        customerDTO1.setCountryCode(351);
        customerDTO1.setName(customer1.getName());
        customerDTO1.setCountryName("Portugal");

        CustomerDTO customerDTO2 = new CustomerDTO();
        customerDTO2.setCountryCode(352);
        customerDTO2.setName(customer2.getName());
        customerDTO2.setCountryName("Spain");

        listCustomersDTO.add(customerDTO1);
        listCustomersDTO.add(customerDTO2);

        when(this.customerConverter.convertCustomerEntityToDto(customer1))
                .thenReturn(customerDTO1);

        // when
        ResponseEntity<List<CustomerDTO>> result = customerController.fetchAllCustomer();

        // then
        assertThat(result.getBody().size()).isEqualTo(2);
        assertEquals("Ze Pincel", result.getBody().get(0).getName());

    }

    @Test
    void fetchCustomerByFilterPhoneCodeTest() throws CustomerNotFoundException {
        filter.setCode("237");
        Long totalElements = 0l;
        int totalPages = 0;
        int number = 0;
        int size = 0;

        // create customer list
        lstCustomer.add(new Customer());
        lstCustomer.add(new Customer());
        lstCustomer.add(new Customer());
        lstCustomer.add(new Customer());
        lstCustomer.add(new Customer());

        final PageImpl page = new PageImpl<>(lstCustomer);
        final Pageable pageable = pageRequest;

        when(this.customerService.fetchCustomerByFilter(filter, pageable))
                .thenReturn(page);

        // create a list of customersDto
        CustomerDTO customerDTO1 = new CustomerDTO();
        customerDTO1.setCountryCode(351);
        customerDTO1.setCountryName("Portugal");

        CustomerDTO customerDTO2 = new CustomerDTO();
        customerDTO2.setCountryCode(352);
        customerDTO2.setCountryName("Spain");

        listCustomersDTO.add(customerDTO1);
        listCustomersDTO.add(customerDTO2);

        PageDTO pageDTO = new PageDTO(listCustomersDTO, totalElements, totalPages, number, size);

        when(this.customerConverter.convertPageCustomersEntityToDto(filter, page))
                .thenReturn(pageDTO);

        ResponseEntity<PageDTO<CustomerDTO>> responseEntity = this.customerController.fetchCustomerByFilter(filter, pageable);
        assertThat(responseEntity.getBody().getContent().size()).isGreaterThan(0);
        assertEquals("Portugal", responseEntity.getBody().getContent().get(0).getCountryName());

    }

    @Test
    void fetchCustomerByFilterValidPhoneCodeTest() throws CustomerNotFoundException {

        filter.setValid(true);

        Long totalElements = 0l;
        int totalPages = 0;
        int number = 0;
        int size = 0;

        Customer cMorocco = new Customer();
        cMorocco.setId(1);
        cMorocco.setName("Yosaf Karrouch");
        cMorocco.setPhone("(212) 698054317");

        Customer cMozambique = new Customer();
        cMozambique.setId(2);
        cMozambique.setName("Edunildo Gomes Alberto");
        cMozambique.setPhone("(258) 847651504");

        Customer cUganda = new Customer();
        cUganda.setId(3);
        cUganda.setName("JACKSON NELLY");
        cUganda.setPhone("(256) 775069443");

        // create customer list
        lstCustomer.add(cMorocco);
        lstCustomer.add(cMozambique);
        lstCustomer.add(cUganda);

        // create an invalid list
        Customer cMoroccoInvalid = new Customer();
        cMoroccoInvalid.setId(4);
        cMoroccoInvalid.setName("Walid Hammadi");
        cMoroccoInvalid.setPhone("(212) 6007989253");

        lstCustomer.add(cMoroccoInvalid);

        Customer cMozambiqueInvalid = new Customer();
        cMozambiqueInvalid.setId(5);
        cMozambiqueInvalid.setName("Tanvi Sachdeva");
        cMozambiqueInvalid.setPhone("(258) 84330678235");

        lstCustomer.add(cMoroccoInvalid);

        final PageImpl page = new PageImpl<>(lstCustomer);
        final Pageable pageable = pageRequest;

        when(this.customerService.fetchCustomerByFilter(filter, pageable))
                .thenReturn(page);

        // create a list of customersDto
        CustomerDTO customerDTO1 = new CustomerDTO();
        customerDTO1.setName("Yosaf Karrouch");
        customerDTO1.setStateValid(true);
        customerDTO1.setCountryCode(212);
        customerDTO1.setCountryName("Morocco");

        CustomerDTO customerDTO2 = new CustomerDTO();
        customerDTO1.setName("Edunildo Gomes Alberto");
        customerDTO1.setStateValid(true);
        customerDTO2.setCountryCode(258);
        customerDTO2.setCountryName("Mozambique");

        CustomerDTO customerDTO3 = new CustomerDTO();
        customerDTO1.setName("JACKSON NELLY");
        customerDTO1.setStateValid(true);
        customerDTO3.setCountryCode(256);
        customerDTO3.setCountryName("Uganda");

        listCustomersDTO.add(customerDTO1);
        listCustomersDTO.add(customerDTO2);
        listCustomersDTO.add(customerDTO3);

        PageDTO pageDTO = new PageDTO(listCustomersDTO, totalElements, totalPages, number, size);


        when(this.customerConverter.convertPageCustomersEntityToDto(filter, page))
                .thenReturn(pageDTO);

        ResponseEntity<PageDTO<CustomerDTO>> responseEntity = this.customerController.fetchCustomerByFilter(filter, pageable);
        assertThat(responseEntity.getBody().getContent().size()).isEqualTo(3);
        assertEquals("Morocco", responseEntity.getBody().getContent().get(0).getCountryName());
        assertEquals("Mozambique", responseEntity.getBody().getContent().get(1).getCountryName());
        assertEquals("Uganda", responseEntity.getBody().getContent().get(2).getCountryName());

    }

}