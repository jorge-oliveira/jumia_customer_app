package pt.jorge.jumiaservice.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import pt.jorge.jumiaservice.dto.CountryDTO;
import pt.jorge.jumiaservice.service.CountryService;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@AutoConfigureMockMvc
//@WebMvcTest // loads the application context
class CountryControllerTest {

    private final String countriesJson = "[{\"code\":237,\"name\":\"Cameroon\"}" +
            ",{\"code\":251,\"name\":\"Ethiopia\"},{\"code\":212,\"name\":\"Morocco\"},{\"code\":258,\"name\":\"Mozambique\"}," +
            "{\"code\":256,\"name\":\"Uganda\"}]";

    @Autowired
    private MockMvc mockMvc;

    @Mock
    CountryService countryService;

    @InjectMocks
    private CountryController countryController;

    @BeforeEach
    void setUp() { }

    @Test
    void countryUrlTest() throws Exception {
        this.mockMvc.perform(get("/api/v1/country"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void listAllCountriesSizeTest(){

        // given
        CountryDTO country1 = new CountryDTO();
        country1.setCode(351);
        country1.setName("Portugal");

        CountryDTO country2 = new CountryDTO();
        country2.setCode(352);
        country2.setName("Spain");

        List<CountryDTO> countries = new ArrayList<>();
        countries.add(country1);
        countries.add(country2);

        when(countryService.getAllCountries()).thenReturn(countries);

        // when
        ResponseEntity<List<CountryDTO>> result = countryController.listAllCountries();

        // then
        assertThat(result.getBody().size()).isEqualTo(2);

        assertThat(result.getBody().get(0).getName())
                .isEqualTo(country1.getName());

        assertThat(result.getBody().get(1).getName())
                .isEqualTo(country2.getName());
    }

    @Test
    void listAllCountriesValidTest() throws Exception {
        this.mockMvc.perform(get("/api/v1/country"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(countriesJson)));
    }


}