package pt.jorge.jumiaservice.converter;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import pt.jorge.jumiaservice.dto.CustomerDTO;
import pt.jorge.jumiaservice.dto.PageDTO;
import pt.jorge.jumiaservice.model.Customer;
import pt.jorge.jumiaservice.model.SearchFilter;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@AutoConfigureMockMvc
class CustomerConverterTest {

    @Autowired
    CustomerConverter customerConverter;

    private final List<CustomerDTO> listCustomersDTO = new ArrayList<>();
    private final List<Customer> lstCustomer = new ArrayList<>();
    private PageRequest pageRequest;
    private SearchFilter filter;

    @BeforeEach
    void setUp() {
        pageRequest = PageRequest.of(0, 1, Sort.by("id"));
        filter = new SearchFilter();
    }

    @Test
    void convertCustomerEntityToDtoTest() {

        Customer customer = new Customer();
        customer.setId(1);
        customer.setName("Yosaf Karrouch");
        customer.setPhone("(212) 698054317");

        CustomerDTO customerDTO = new CustomerDTO();
        customerDTO.setCountryCode(212);
        customerDTO.setName(customer.getName());
        customerDTO.setCountryName("Morocco");
        customerDTO.setStateValid(true);
        customerDTO.setPhoneNumber("(212) 698054317");

        CustomerDTO customerDTOConverted = this.customerConverter.convertCustomerEntityToDto(customer);
        assertEquals(customerDTO.getName().toUpperCase(), customerDTOConverted.getName());
        assertEquals(customerDTO.getPhoneNumber(), customerDTOConverted.getPhoneNumber());
        assertEquals(customerDTO.getCountryCode(), customerDTOConverted.getCountryCode());
        assertEquals(customerDTO.getCountryName(), customerDTOConverted.getCountryName());
        assertEquals(customerDTO.isStateValid(), customerDTOConverted.isStateValid());

    }

    @Test
    void convertPageCustomersEntityToDtoTest() {

        generateListCustomers();

        final PageImpl page = new PageImpl<>(lstCustomer);
        final Pageable pageable = pageRequest;

        PageDTO pageDTO = this.customerConverter.convertPageCustomersEntityToDto(filter, page);
        assertEquals(5, pageDTO.getContent().size());

    }

    @Test
    void convertPageCustomersEntityToDtoValidStateTest() {

        generateListCustomers();

        final PageImpl page = new PageImpl<>(lstCustomer);
        final Pageable pageable = pageRequest;
        filter.setValid(true);

        PageDTO pageDTO = this.customerConverter.convertPageCustomersEntityToDto(filter, page);
        assertEquals(3, pageDTO.getContent().size());

    }

    @Test
    void convertPageCustomersEntityToDtoInvalidStateTest() {

        generateListCustomers();

        final PageImpl page = new PageImpl<>(lstCustomer);
        final Pageable pageable = pageRequest;
        filter.setValid(false);

        PageDTO pageDTO = this.customerConverter.convertPageCustomersEntityToDto(filter, page);
        assertEquals(2, pageDTO.getContent().size());
    }

    private void generateListCustomers() {
        Customer cMorocco = new Customer();
        cMorocco.setId(1);
        cMorocco.setName("Yosaf Karrouch");
        cMorocco.setPhone("(212) 698054317");

        Customer cMozambique = new Customer();
        cMozambique.setId(2);
        cMozambique.setName("Edunildo Gomes Alberto");
        cMozambique.setPhone("(258) 847651504");

        Customer cUganda = new Customer();
        cUganda.setId(3);
        cUganda.setName("JACKSON NELLY");
        cUganda.setPhone("(256) 775069443");

        // create customer list
        lstCustomer.add(cMorocco);
        lstCustomer.add(cMozambique);
        lstCustomer.add(cUganda);

        // create an invalid list
        Customer cMoroccoInvalid = new Customer();
        cMoroccoInvalid.setId(4);
        cMoroccoInvalid.setName("Walid Hammadi");
        cMoroccoInvalid.setPhone("(212) 6007989253");

        lstCustomer.add(cMoroccoInvalid);

        Customer cMozambiqueInvalid = new Customer();
        cMozambiqueInvalid.setId(5);
        cMozambiqueInvalid.setName("Tanvi Sachdeva");
        cMozambiqueInvalid.setPhone("(258) 84330678235");

        lstCustomer.add(cMoroccoInvalid);

    }

}