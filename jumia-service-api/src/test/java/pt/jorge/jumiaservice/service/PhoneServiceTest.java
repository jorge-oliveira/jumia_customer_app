package pt.jorge.jumiaservice.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class PhoneServiceTest {

    @Autowired
    PhoneService phoneService;

    @BeforeEach
    void setUp() {
    }

    @Test
    void extractValidPhoneCodeTest() {

        String code = phoneService.extractPhoneCode("(212) 6007989253");
        assertEquals("212", code);
    }

    @Test
    void extractInvalidPhoneCodeTest() {

        String code = phoneService.extractPhoneCode("( 6007989253");
        assertEquals(null, code);
    }
}