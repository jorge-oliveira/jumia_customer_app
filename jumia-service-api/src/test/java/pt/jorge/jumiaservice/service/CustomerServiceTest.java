package pt.jorge.jumiaservice.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.*;
import pt.jorge.jumiaservice.exception.CustomerNotFoundException;
import pt.jorge.jumiaservice.model.Customer;
import pt.jorge.jumiaservice.model.SearchFilter;
import pt.jorge.jumiaservice.repository.CustomerRepository;
import pt.jorge.jumiaservice.service.impl.CustomerServiceImpl;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest
@AutoConfigureMockMvc
class CustomerServiceTest {

    @Mock
    private CustomerRepository customerRepository;

    @InjectMocks
    private CustomerServiceImpl customerService;

    private List<Customer> lstCustomers;
    private PageRequest pageRequest;
    private SearchFilter filter;

    @BeforeEach
    void setUp() {
        lstCustomers = new ArrayList<>();
        pageRequest = PageRequest.of(0, 1, Sort.by("id"));
        filter = new SearchFilter();
    }

    @Test
    void getAllCustomersTest() {

        lstCustomers.add(new Customer());
        lstCustomers.add(new Customer());

        when(this.customerRepository.findAll())
                .thenReturn(lstCustomers);

        List<Customer> allCustomers = customerService.fetchAllCustomer();
        assertEquals(2, allCustomers.size());
    }

    @Test
    void getCustomersByFilterPageableTest() throws CustomerNotFoundException {

        lstCustomers.add(new Customer());
        lstCustomers.add(new Customer());
        lstCustomers.add(new Customer());
        lstCustomers.add(new Customer());
        lstCustomers.add(new Customer());

        final PageImpl page = new PageImpl<>(lstCustomers);
        final Pageable pageable = pageRequest;

        filter.setCode("237");
        when(this.customerRepository.findAllCustomersByPhoneNumberCode(filter.getCode(), pageRequest))
                .thenReturn(page);

        Page<Customer> customerByFilterPageable = customerService.fetchCustomerByFilter(filter, pageable);
        assertThat(customerByFilterPageable.getContent().size()).isGreaterThan(0);
        assertEquals(5, customerByFilterPageable.getContent().size());

    }

    @Test
    void getCustomerByFilterPageableWithExceptionTest() {

        filter.setCode("xxxxx");

        assertThrows(CustomerNotFoundException.class, () -> {
            Page<Customer> customerByFilterPageable = customerService.fetchCustomerByFilter(filter, pageRequest);
            assertThat(customerByFilterPageable.getTotalElements()).isGreaterThan(0);
        });
    }

}