package pt.jorge.jumiaservice.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import pt.jorge.jumiaservice.dto.CountryDTO;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
@AutoConfigureMockMvc
class CountryServiceTest {

    @Autowired
    CountryService countryService;

    @BeforeEach
    void setUp() {
    }

    @Test
    void findCountryByCountryCodeTest() {

        Integer countryCode = 212;

        String country = countryService.findCountry(countryCode);
        assertEquals("Morocco", country);
    }

    @Test
    void getAllCountries() {

        List<CountryDTO> allCountries = countryService.getAllCountries();

        assertNotNull(allCountries);
        assertEquals(5, allCountries.size());
        assertEquals("Uganda", allCountries.get(4).getName());
    }
}