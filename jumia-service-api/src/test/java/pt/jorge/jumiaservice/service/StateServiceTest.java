package pt.jorge.jumiaservice.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class StateServiceTest {

    @Autowired
    StateService stateService;

    private String country;
    private String phoneNumber;

    @BeforeEach
    void setUp() {
        this.phoneNumber = "(256) 775069443";
        this.country = "Uganda";
    }

    @Test
    void validateTheStateOfValidPhoneNumberTest() {
        boolean validState = stateService.validateState(country, phoneNumber);
        assertEquals(true, validState);
    }

    @Test
    void validateTheStateOfInvalidPhoneNumberTest() {
        this.country = "Morocco";
        this.phoneNumber = "(212) 6007989253";
        boolean invalidState = stateService.validateState(country, phoneNumber);
        assertEquals(false, invalidState);
    }

    @Test
    void validateTheStateOfInvalidCountryNameTest() {
        this.country = "Portugal";
        this.phoneNumber = "(351) 123123123";
        boolean invalidState = stateService.validateState(country, phoneNumber);
        assertEquals(false, invalidState);
    }
}