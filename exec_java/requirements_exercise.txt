Hello again Jorge,


Welcome to the 2nd stage of the interview process :)


It starts with a programming exercise.



The deadline is 1 week (7 days counting from tomorrow), but the sooner you finish the sooner we can move forward with your process.

 
This project requires the implementation of a basic business logic related to the validation of international phone numbers.



The business logic is purposely simple, and our focus will be on the quality of the code, e.g., architecture, cleanliness, unit testing, etc.



Make sure you include the FE, the BE, and a complete readme so that we know how to boot up the project.



On this last point, please make sure the project includes a docker file.



Finally, in terms of using frameworks, please use spring boot for the backend. We'll let you pick any for the front end.



We send a zip file with the project description and a SQLite database with sample data in the attachment. 


Please reply to me and all people on CC and submit using a GitHub repository.



Feel free to ask questions by replying to this thread. 

Good luck!

Best regards,



José Martins