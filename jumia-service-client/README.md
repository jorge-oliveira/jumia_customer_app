# JumiaServiceClient

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.2.2.
This is a client for the API project.
The project should be running before execute this one

Execute the docker compose file to build the images.

## Open app
http://localhost:4200/customer

---

## Docker
This application contains a Dockerfile

#### Build Container
* docker build -t jumia-service-client .

#### Start the container
* docker run -p 4200:80 jumia-service-client
  docker run -d -p 4200:80 jumia-service-client


---

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

