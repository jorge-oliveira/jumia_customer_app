import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Customer} from "../model/customer";
import {Country} from "../model/country";
import {CustomerResponse} from "../model/CustomerResponse";

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  private REST_API_SERVER = "http://localhost:8081/api/v1";
  // private REST_API_SERVER = "http://localhost:9000/api/v1";

  constructor(private httpClient: HttpClient) {
  }

  getAllCustomers(): Observable<Customer[]> {
    return this.httpClient.get<Customer[]>(this.REST_API_SERVER + "/customer");
  }

  listCustomersByFilterPageable(params: any): Observable<CustomerResponse> {
    const headers = {'Authorization': 'Bearer my-token', 'My-Custom-Header': 'foobar'};
    let body = {};
    if (params.state !== null) {
      body = {
        code: params.code,
        valid: params.state,
      }
    } else {
      body = {code: params.code}
    }
    return this.httpClient
      .post<CustomerResponse>(this.REST_API_SERVER + "/customer?page="+ params.page +"&size=" + params.size,
        body, {headers});
  }

  getCountries(): Observable<Country[]> {
    return this.httpClient.get<Country[]>(this.REST_API_SERVER + "/country");
  }
}
