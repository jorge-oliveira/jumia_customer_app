import {Component, OnInit} from '@angular/core';
import {Customer} from "../model/customer";
import {CustomerService} from "../service/customer.service";
import {Country} from "../model/country";
import {CustomerResponse} from "../model/CustomerResponse";
import {PaginationInstance} from "ngx-pagination";

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit {

  customers: Customer [] = [];
  countries: Country [] = [];
  states: any = [];
  code: any;
  state: any;

  page: number = 0;
  total: number = 50;
  // tableSize:number = 10;
  loading: boolean = false;

  public config: PaginationInstance = {
    id: 'server',
    itemsPerPage: 10,
    currentPage: this.page,
    totalItems: this.total
  };

  // tableSize = 7;
  tableSizes = [10, 20, 30, 40, 50];
  currentIndex: any;

  constructor(private customerService: CustomerService) {
    this.states = [
      {id: 1, name: "Valid"},
      {id: 2, name: "Invalid"},
    ]
  }

  ngOnInit(): void {
    this.listCountries();
    this.listCustomersByFilterPageable();
  }

  /**
   * Get list of all Countries
   */
  listCountries() {
    this.customerService.getCountries()
      .subscribe({
        next: (data: Country[]) => {
          // console.log("Response API: ", data);
          this.countries = data;
        },
        error: (e) => {
          console.error(e);
          if (e.status === 404) {
            console.log("Error Listing Countries: ", e.status);
          }
        }
      });
  }


  listCustomersByFilterPageable() {
    this.loading = true;

    let params = {
      code: this.code,
      state: this.state,
      page: this.config.currentPage,
      size: this.config.itemsPerPage
    }

    console.info("params: ", params);

    this.customerService.listCustomersByFilterPageable(params)
      .subscribe({
        next: (data: CustomerResponse) => {
          console.log("Response API: ", data);
          this.customers = data.content;

          console.info("Current page: ", this.config.currentPage);
          this.config.currentPage = data.customPage.number;
          this.config.totalItems = data.customPage.totalElements;
          this.config.itemsPerPage = data.customPage.size;

          this.loading = false;
        },
        error: (e) => {
          console.error(e);
          if (e.status === 404) {
            console.log("Error: ", e.status);
          }
        }
      });

  }

  /**
   * Search for the customers
   */
  searchCustomerCode() {
    if (typeof this.state === 'undefined') {
      this.state = null;
    }
    this.listCustomersByFilterPageable();
  }


  selectCountry(e: any) {

    const selectedCategoryArray = this.countries
      .filter((country) =>
        country.name === e.target.value
      );

    this.code = selectedCategoryArray[0].code;
    console.log("Code: ", this.code);
  }

  selectState(e: any) {

    console.info("State: ", e.target.value);
    if (e.target.value.toUpperCase() === 'VALID') {
      this.state = true;
    } else if (e.target.value.toUpperCase() === 'INVALID') {
      this.state = false;
    } else {
      this.state = null;
    }
  }

  changePaginationConfig(page: any) {
    console.log("PAGE: ", page);
    this.page = page;

    this.config.currentPage = page;
    this.listCustomersByFilterPageable();
  }

  onTableSizeChange(event: any): void {
    // console.log("itemsPerPage: ", event.target.value);
    this.config.itemsPerPage = event.target.value;
    this.listCustomersByFilterPageable();
  }
}
