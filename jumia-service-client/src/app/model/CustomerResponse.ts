import {Customer} from "./customer";
import {CustomPage} from "./customPage";

export interface CustomerResponse{
  content: Customer [];
  customPage: CustomPage;
}
