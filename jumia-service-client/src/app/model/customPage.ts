export interface CustomPage {
  number: number;
  size: number;
  totalElements: number;
  totalPages: number;

}
