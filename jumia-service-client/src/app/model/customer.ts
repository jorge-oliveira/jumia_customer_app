export interface Customer {
  id:number;
  name: string;
  phoneNumber: string;
  countryCode:number;
  countryName:string;
  stateValid:boolean;
}
