# README #

Start the application

### Docker ###

#### build the images
* docker-compose up

---
### Access the apps

#### Frontend

Open in browser the link.

* [customer-client](http://localhost:4200/customer)

#### Backend
The API as embedded the Swagger-ui for testing.

* [swagger-api](http://localhost:8081/api/swagger/swagger-ui/index.html)

---
### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact